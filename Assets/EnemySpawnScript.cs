﻿using UnityEngine;
using System.Collections;

public class EnemySpawnScript : MonoBehaviour {

    public bool spawning = false;
    public Transform enemyPlane;
    public Vector3 enemySpawnpoint;
    public static int enemiesSpawned = 0;

	void Start () {
	}

    IEnumerator SpawnEnemy()
    {
        yield return new WaitForSeconds(Random.Range(1.0f - SpawnFrequencyMin(), 1.7f - SpawnFrequencyMax()));
        spawning = false;
	    Instantiate(enemyPlane, CreateRandomSpawnpoint(), Quaternion.identity);
        enemiesSpawned++;
        
	}

    void Update()
    {

        if (!spawning)
        {
            spawning = true;
            StartCoroutine(SpawnEnemy());
        }
    }

    Vector3 CreateRandomSpawnpoint()
    {
        return new Vector3(Random.Range(-19.0f, 19.0f), -5.61f, 3.49f);
    }


    float SpawnFrequencyMin()
    {
        float x = 0.025f * enemiesSpawned;
        if (x > 0.9f)
        {
            x = 0.9f;
        }
        return x;
    }

    float SpawnFrequencyMax()
    {
        float x = 0.025f * enemiesSpawned;
        if (x > 1.55f)
        {
            x = 1.55f;
        }
        return x;
    }
}