﻿using UnityEngine;
using System.Collections;

public class DeathScript : MonoBehaviour {



    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy" || other.gameObject.tag == "EnemyPlasma")
        {
            Destroy(other.gameObject);
            if (other.gameObject.tag == "Enemy")
            {
                GUIScript1.score++;
            }
        }
    }

}
