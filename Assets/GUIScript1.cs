﻿using UnityEngine;
using System.Collections;

public class GUIScript1 : MonoBehaviour {

    public static int score = 0;
    public GUISkin skin;

	void OnGUI()
    {
        GUI.skin = skin;
        GUI.Label(new Rect(0,0,100,50), "Score: " +score);

        if (ShipController.reloading) {

            GUI.Label(new Rect(Screen.width - 100, 0, 100, 50), "RELOADING");
        }
        else {

            GUI.Label(new Rect(Screen.width - 100, 0, 100, 50), "Clip: " + ShipController.shotsLeft);
        }
    }

}
