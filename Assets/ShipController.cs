﻿using UnityEngine;
using System.Collections;

public class ShipController : MonoBehaviour {

    public float speedFactor = 80.0f;
    public float shotSpeedFactor = 3.0f;
    public Transform plasmaShot;
    public Transform shotSpawnpoint;
    bool wait = false;
    public static bool reloading = false;
    public static int clipSize = 10;
    public static int shotsLeft = clipSize;
    public static int reloadTime = 5;


	void Start () {
	
	}
	
	void FixedUpdate () {
        if (Input.GetAxis("Horizontal") < 0.0f)
        {
            rigidbody.AddForce(-transform.right*speedFactor);
        }
        else if (Input.GetAxis("Horizontal") > 0.0f)
        {
            rigidbody.AddForce(transform.right * speedFactor);
        }

        if (Input.GetAxis("Vertical") < 0.0f)
        {
            rigidbody.AddForce(-transform.up * speedFactor);
        }
        else if (Input.GetAxis("Vertical") > 0.0f)
        {
            rigidbody.AddForce(transform.up * speedFactor);
        }


        
	}

    void OnTriggerEnter (Collider other) {
        if (other.gameObject.tag == "Enemy" || other.gameObject.tag == "EnemyPlasma")
        {
            Destroy(other.gameObject);
            Destroy(this.gameObject);
            Application.LoadLevel("DeathScene");
        }
    }


    IEnumerator WaitBetweenShots()
    {
        yield return new WaitForSeconds(0.2f);
        wait = false;
    }

    IEnumerator Reload()
    {
        yield return new WaitForSeconds(reloadTime);
        shotsLeft = clipSize;
        reloading = false;
    }


    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            if (!wait && !reloading)
            {
                Instantiate(plasmaShot, shotSpawnpoint.position, Quaternion.identity);
                shotsLeft--;
                wait = true;
                StartCoroutine(WaitBetweenShots());
                if (shotsLeft == 0)
                {
                    reloading = true;
                    StartCoroutine(Reload());
                }
            }
        }
    }
}
