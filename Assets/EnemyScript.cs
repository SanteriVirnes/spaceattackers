﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour
{

    public float enemySpeed = 3.0f;
    public float sideSpeed = 3.0f;
    public float sideForceDirection;
    bool enemyWait = false;
    bool sideForceOn = false;
    float sideForceOrNot = Random.Range(0.0f, 2.0f);
    public Transform EnemyShotSpawnpoint;
    public Transform EnemyPlasma;

    IEnumerator SideForce()
    {
        sideForceDirection = Random.Range(0.0f, 2.0f);
        yield return new WaitForSeconds(2);
        sideForceOn = false;
    }

    void Update()
    {
        rigidbody.AddForce(-Vector3.forward * enemySpeed);

        if (sideForceOrNot >= 0 && sideForceOrNot <= 1)
        {
            if (!sideForceOn)
            {
                sideForceOn = true;
                StartCoroutine(SideForce());
            }
            if (sideForceDirection >= 0 && sideForceDirection <= 1)
            {
                rigidbody.AddForce(Vector3.left * sideSpeed);
            }
            if (sideForceDirection >= 1 && sideForceDirection <= 2)
            {
                rigidbody.AddForce(-Vector3.left * sideSpeed);
            }
        }


        if (!enemyWait)
        {
            enemyWait = true;
            StartCoroutine(EnemyWaitBeforeShooting());
        }

    }

    IEnumerator EnemyWaitBeforeShooting()
    {
        yield return new WaitForSeconds(Random.Range(1.0f, 2.0f));
        Instantiate(EnemyPlasma, EnemyShotSpawnpoint.position, Quaternion.identity);
        enemyWait = false;
    }

}