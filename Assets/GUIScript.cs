﻿using UnityEngine;
using System.Collections;

public class GUIScript : MonoBehaviour {

	public int plasma = 3;
	public int score = 10000;
	public GUISkin skin;

	void OnGUI()
	{
		GUI.skin = skin;
		GUI.Label(new Rect(Screen.width-100,0,100,50), "Score: " + score);
		GUI.Label(new Rect(0,0,100,50), "Ammukset: " + plasma);
	}
}
