﻿using UnityEngine;
using System.Collections;

public class PlasmaScript : MonoBehaviour {

    public float shotSpeedFactor = 5.0f;
    public float timeToLiveSeconds = 15.0f;
    float aliveSeconds = 0.0f;
    

	// Use this for initialization
	void Start ()
    {
        rigidbody.velocity = Vector3.forward * shotSpeedFactor;
    }
	
	// Update is called once per frame
	void Update ()
    {

        aliveSeconds += Time.deltaTime;
        if (aliveSeconds > timeToLiveSeconds)
        {
            Destroy(this.gameObject);
        }

        
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            Destroy(other.gameObject);
            Destroy(this.gameObject);
            GUIScript1.score += 5;
        }
    }

    
}
